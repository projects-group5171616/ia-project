import mysql.connector
from mysql.connector import Error

def crear_conexion():
    """Establece una conexión con la base de datos MySQL."""
    try:
        conexion = mysql.connector.connect(
            host='localhost',
            user='root',
            password='',
            database='bd_soccer'
        )
        if conexion.is_connected():
            print("Conexión exitosa a la base de datos")
            return conexion
    except Error as e:
        print(f"Error al conectar a la base de datos: {e}")
        return None

def cerrar_conexion(conexion):
    """Cierra la conexión con la base de datos MySQL."""
    if conexion.is_connected():
        conexion.close()
        print("Conexión cerrada")