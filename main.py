from aifc import Error
from conexion import crear_conexion, cerrar_conexion

def mostrar_menu():
    print("=== MENÚ PRINCIPAL ===")
    print("1. Opción 1")
    print("2. Opción 2")
    print("3. Opción 3")
    print("4. Salir")

def opcion1(conexion):
    cursor = conexion.cursor()
    try:
        cursor.execute("SELECT * FROM team")
        resultados = cursor.fetchall()
        for fila in resultados:
            print(fila)
    except Error as e:
        print(f"Error al obtener los equipos: {e}")

def opcion2():
    print("Has seleccionado la Opción 2")

def opcion3():
    print("Has seleccionado la Opción 3")

def menu():
    while True:
        mostrar_menu()
        opcion = input("Selecciona una opción: ")
        
        if opcion == '1':
            conexion = crear_conexion()
            opcion1(conexion)
        elif opcion == '2':
            opcion2()
        elif opcion == '3':
            opcion3()
        elif opcion == '4':
            print("Saliendo del programa...")
            break
        else:
            print("Opción no válida, por favor intenta de nuevo.")

if __name__ == "__main__":
    menu()